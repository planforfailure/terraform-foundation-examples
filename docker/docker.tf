provider "docker" {}

resource "docker_image" "nginx" {
  name = "nginx:latest"
}

resource "docker_container" "web" {
  name  = "web-server"
  image = docker_image.nginx.latest

  ports {
    internal = 80
    external = 80
  }
  ports {
    internal = 443
    external = 443
  }
}
