resource "aws_vpc" "my1st_vpc" {
  cidr_block           = "10.121.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "dev"
  }
}

resource "aws_subnet" "my1st_subnet" {
  vpc_id                  = aws_vpc.my1st_vpc.id
  cidr_block              = "10.121.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "eu-west-2a"

  tags = {
    Name = "dev-public"
  }
}

resource "aws_internet_gateway" "my1st_internet_gateway" {
  vpc_id = aws_vpc.my1st_vpc.id
  tags = {
    Name = "dev-igw"

  }

}

resource "aws_route_table" "my1st_route_table" {
  vpc_id = aws_vpc.my1st_vpc.id

  tags = {
    Name = "dev-public-rt"
  }
}

resource "aws_route" "default_route" {
  route_table_id         = aws_route_table.my1st_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my1st_internet_gateway.id
}

resource "aws_route_table_association" "my1st_public_assoc" {
  subnet_id      = aws_subnet.my1st_subnet.id
  route_table_id = aws_route_table.my1st_route_table.id

}

resource "aws_security_group" "my1st_sg" {
  name        = "dev_sg"
  description = "dev secuirty group"
  vpc_id      = aws_vpc.my1st_vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_key_pair" "my1st_auth" {
  key_name   = "terraform_aws"
  public_key = file("~/.ssh/terraform_aws.pub")

}

resource "aws_instance" "dev_node" {
  instance_type = "t2.micro"
  ami           = data.aws_ami.server_ami.id
  key_name               = aws_key_pair.my1st_auth.id
  vpc_security_group_ids = [aws_security_group.my1st_sg.id]
  subnet_id              = aws_subnet.my1st_subnet.id
  user_data = file("userdata.tpl")

  root_block_device {
    volume_size = 10
  }

    tags = {
    Name = "dev_node"
  }


  provisioner "local-exec" {
    command = templatefile("${var.host_os}-ssh-config.tpl", {
      hostname = self.public_ip,
      user     = "ubuntu",
    identityfile = "~/.ssh/terraform_aws" })
    interpreter = var.host_os == "linux" ? ["bash", "-c"] : ["Powershell", "-Command"]
  }

}