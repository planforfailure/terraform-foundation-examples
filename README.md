# Terraform Foundations Course
https://www.youtube.com/watch?v=wZoEpn6DwmY&list=PL5_Rrj9tYQAlgX9bTzlTN0WzU67ZeoSi_&index=1
 - https://www.youtube.com/watch?v=SLB_c_ayRMo  # additional course

https://github.com/morethancertified/rfp-terraform



![alt text](/images/build_environment.png "Terraform Build")


## Installation

Terraform files  _.tf_ can be split up into separate files as you see fit, provided they are all in the same directory and are treated as one when running terraform commands.

https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli  # or via repo


```bash
sudo apt-get update && sudo apt-get install -y gnupg software-properties-common

wget -O- https://apt.releases.hashicorp.com/gpg | \
gpg --dearmor | \
sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg > /dev/null

gpg --no-default-keyring \
--keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
--fingerprint

/usr/share/keyrings/hashicorp-archive-keyring.gpg
-------------------------------------------------
pub   rsa4096 XXXX-XX-XX [SC]
AAAA AAAA AAAA AAAA
uid           [ unknown] HashiCorp Security (HashiCorp Package Signing) <security+packaging@hashicorp.com>
sub   rsa4096 XXXX-XX-XX [E]

```

## Setup 

https://registry.terraform.io/providers/hashicorp/aws/latest/docs

#### providers.tf
[providers.tf](project_course/providers.tf)

```hcl
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
    }
  }
}

provider "aws" {
  shared_config_files      = ["~/.aws/config"]
  shared_credentials_files = ["~/.aws/credentials"]
  profile                  = "terraform"
}
```

https://developer.hashicorp.com/terraform/language/state

```bash
terraform init
terraform plan  
terraform apply  

terraform state show aws_vpc.my1st_vpc
terraform state list # shows all resoures
terraform state show aws_instance.dev_node # will show all info about host, including IP

terraform destroy --auto-approve  # without prompting to type yes
terraform destroy -target aws_instance.web-server-instance # targeted removal of an instance, \
can also be used with terraform apply -target...
terraform fmt  #  linting
terraform apply --replace aws_instance.dev_node  # used to be taint when forcing a change, \
use when  using provisioners as they are not picked up by the state file
terraform output # will show you output requested from terraform state 
terraform refresh # similar to above, will not apply anthing, will print output
terraform apply -var "subnet_prefix=10.0.9.50/24"  # when defining variable inline, will prompt if not defined
terraform apply --refresh-only # when you don't want to destoy the instance, and say update utilisting output.tf you would get.

Apply complete! Resources: 0 added, 0 changed, 0 destroyed.

Outputs:

dev_ip = "35.176.137.140"

terraform output # as above

terraform console
terraform console -var="host_os=unix"  # beats default and .tfvars
terraform console -var-file="dev.tfvars"  # supercedes above
terraform console
> aws_instance.dev_node.public_ip
"35.176.137.140"
>  


# Expressions
terraform console -var="host_os=windows"
> var.host_os
"windows"
> var.host_os == "windows" ? "powershell" : "bash"
"powershell"
> var.host_os == "linux" ? "bash" : "powershell"
"bash"

# Add to main.tf for dynamic configuration, updates ~/.ssh/config on the fly and will add a connection in \
Remote SSH in vscode
interpreter = var.host_os == "linux" ? ["bash", "-c"] : ["Powershell", "-Command"]

```
#### main.tf
[main.tf](project_course/main.tf)

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/internet_gateway

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route_table.html

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/main_route_table_association

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/key_pair
- https://developer.hashicorp.com/terraform/language/functions/file

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance


#### datasources.tf
[datasources.tf](project_course/datasources.tf)

https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami

https://developer.hashicorp.com/terraform/language/resources/provisioners/syntax
 - Sucess/Failure is **not** recorded in .tfstate file
 - No rollback available, only option is to re-run it
 - https://developer.hashicorp.com/terraform/language/resources/provisioners/syntax#provisioners-are-a-last-resort

#### variables.tf
[variables.tf](project_course/variables.tf)

https://developer.hashicorp.com/terraform/language/values/variables
- https://developer.hashicorp.com/terraform/language/values/variables#variable-definition-precedence

https://developer.hashicorp.com/terraform/cli/commands/console

#### Expressions
https://developer.hashicorp.com/terraform/language/expressions/conditionals

https://developer.hashicorp.com/terraform/language/values/outputs

#### Vscode Extensions
- HashiCorp Terraform  - Hashicorp
- AWS Toolkit - Amazon Web Services
- Python - Microsoft
- Pylance - Microsoft
- Remote SSH - Microsoft
