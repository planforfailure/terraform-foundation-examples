https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/droplet

https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean


In this example, we use the digitalocean_droplet resource to create a droplet in the New York City 3 region with Ubuntu 20.04 as the OS, 1 vCPU and 1GB of memory. We also use the connection block to configure an SSH connection to the droplet, and the provisioner block to execute a script that installs Docker and runs a Docker container with Nginx, exposing ports 80 and 443.

We also use the digitalocean_firewall resource to create a firewall that allows incoming traffic on ports 80 and 443, and restricts access to only the droplet created by the digitalocean_droplet resource.

Note that you will need to replace the do_token, private_key_path, and region values with appropriate values for your environment. You will also need to have an SSH key set up on your DigitalOcean account and the corresponding private key file on your local machine. Additionally, you may need to configure authentication credentials if you are using a private Docker registry.
