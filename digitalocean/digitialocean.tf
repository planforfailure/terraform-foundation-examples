provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_droplet" "web" {
  image  = "ubuntu-20-04-x64"
  name   = "web-server"
  region = "nyc3"
  size   = "s-1vcpu-1gb"

  connection {
    type        = "ssh"
    user        = "root"
    private_key = file(var.private_key_path)
  }

  provisioner "remote-exec" {
    inline = [
      "apt-get update",
      "apt-get -y install docker.io",
      "docker run -d -p 80:80 -p 443:443 nginx"
    ]
  }
}

resource "digitalocean_firewall" "nginx" {
  name = "nginx-firewall"

  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0"]
  }

  droplet_ids = [digitalocean_droplet.web.id]
}
