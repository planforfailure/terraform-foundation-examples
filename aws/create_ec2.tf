resource "aws_instance" "my_instance" {
  ami           = "AMI"
  instance_type = "INSTANCE-TYPE"
  subnet_id     = aws_subnet.my_subnet.id
  vpc_security_group_ids = [aws_security_group.my_security_group.id]
  key_name      = "KEY-NAME"
  associate_public_ip_address = true
  tags = {
    Name = "my_instance"
  }
}
